package pl.codementors;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Hello world!
 */
public class Main {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("jeden");
        list.add("dwa");
        list.add("trzy");
        list.add("cztery");
        list.add("piec");

        countCharacters(list);
        filter(list, 4);

        Random random = new Random();
        Runnable runnable = () -> {
            for (int i = 0; i < 10; i++) {
                System.out.println(random.nextInt(10));
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


    public static int countCharacters(List<String> list) {

        return list.stream().mapToInt(element -> element.length()).sum();
    }

    public static List<String> filter(List<String> list, int treshold) {
        return list.stream().filter(element -> (element.length() > treshold)).collect(Collectors.toList());
    }


}