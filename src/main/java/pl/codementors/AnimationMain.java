package pl.codementors;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.animation.FillTransition;
import javafx.animation.Transition;
import javafx.scene.shape.Shape;
import javafx.util.Duration;

public class AnimationMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Group root = new Group();
        Rectangle rectangle1 = new Rectangle(50, 50);

        Button przycisk = new Button();
        przycisk.setLayoutX(0);
        przycisk.setLayoutY(0);
        przycisk.setMinHeight(50);
        przycisk.setMinWidth(50);
        przycisk.setText("move");


        rectangle1.setX(225);
        rectangle1.setY(225);

        rectangle1.setFill(Color.BLUE);

        root.getChildren().add(rectangle1);
        root.getChildren().add(przycisk);

        Transition transition = fillTransition(rectangle1);
        przycisk.setOnAction(event -> {transition.play();});

        Scene scene = new Scene(root, 500, 500,  Color.GREEN);
        stage.setScene(scene);
        stage.show();

    }
    private Transition fillTransition(Shape shape) {
        FillTransition ft = new FillTransition();
        ft.setFromValue(Color.RED);
        ft.setToValue(Color.YELLOW);
        ft.setShape(shape);
        ft.setCycleCount(2);
        ft.setAutoReverse(true);
        ft.setDuration(Duration.millis(1000));
        return ft;
    }
}